import customAxios from "../http";

export default {
    get : async(id) => {
        return await customAxios.get(`/participations/${id}`)
    },
    getAll : async() => {
        return await customAxios.get('/participations')
    },
    create : async(employe) => {
        return await customAxios.post('/participations/',employe)
    },
    update: async (id,employe) => {
        return await customAxios.put(`/participations/${id}`, employe)
    },
    delete : async(id) => {
        return await customAxios.delete(`/participations/${id}`)
    }
}