import customAxios from "../http";

export default {
    get : async(id) => {
        return await customAxios.get(`/formations/${id}`)
    },
    getAll : async() => {
        return await customAxios.get('/formations')
    },
    create : async(employe) => {
        return await customAxios.post('/formations/',employe)
    },
    update: async (id,employe) => {
        return await customAxios.put(`/formations/${id}`, employe)
    },
    delete : async(id) => {
        return await customAxios.delete(`/formations/${id}`)
    }
}