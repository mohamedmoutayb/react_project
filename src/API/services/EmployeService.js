import customAxios from "../http";

export default {
    get : async(id) => {
        return await customAxios.get(`/employes/${id}`)
    },
    getAll : async() => {
        return await customAxios.get('/employes')
    },
    create : async(employe) => {
        return await customAxios.post('/employes/',employe)
    },
    update: async (id,employe) => {
        return await customAxios.put(`/employes/${id}`, employe)
    },
    delete : async(id) => {
        return await customAxios.delete(`/employes/${id}`)
    }
}