import { Link, Outlet } from "react-router-dom";

export default function App() {
  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">
            Logo
          </a>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link" to={"/employes"}>
                  Employe
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/formations"}>
                  Formation
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/participations"}>
                  Participation
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <Outlet />
    </>
  );
}
