import { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addParticipation } from "../../actions/participations/actions";
import { updateEmploye } from "../../actions/employes/actions";
import Select from "react-select";

function AddFormation() {
  const participations = useSelector(
    (state) => state.participations.participations
  );
  const employes = useSelector((state) => state.employes.employes);
  const formations = useSelector((state) => state.formations.formations);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const employe_id = useRef();
  const formation_id = useRef();
  const currentDate = new Date();
  const year = currentDate.getFullYear();
  const month = currentDate.getMonth() + 1;
  const day = currentDate.getDate();
  const [selectedFormations, setSelectedFormations] = useState([]);
  // console.log(employes);

  const nextId = () => {
    if (participations.length === 0) {
      return 1;
    } else {
      return parseInt(Math.max(participations.map((part) => part.id)) + 1);
    }
  };
  const handlesubmit = (e) => {
    e.preventDefault();
    const data = {
      id: nextId(),
      employe_id: employe_id.current.value,
      formation_id: selectedFormations.map((option) => option.value),
      date_participation: ` ${year}-${month < 10 ? "0" : ""}${month}-${
        day < 10 ? "0" : ""
      }${day}`,
    };
    console.log(data);
    const employe_selected = employes.find(
      (emp) => emp.id == employe_id.current.value
    );
    const {
      cin: vcin,
      diplome: vdiplome,
      id: vid,
      nom: vnom,
      prenom: vprenom,
      telephone: vtelephone,
    } = employe_selected;
    let nmbr_formation_updated =
      employe_selected.nmbr_formation + selectedFormations.length;
    const dataEmploye = {
      id: vid,
      nom: vnom,
      prenom: vprenom,
      diplome: vdiplome,
      cin: vcin,
      telephone: vtelephone,
      nmbr_formation: nmbr_formation_updated,
    };
    console.log(dataEmploye);
    dispatch(addParticipation(data));
    dispatch(updateEmploye(employe_selected.id, dataEmploye));
    navigate("/participations");
  };

  const options = formations
  .filter(formation => formation.status != "Acheevées" && formation.annuler != true)
  .map(formation => ({
    value: formation.id,
    label: formation.intitule
  }));

  console.log(options);
  return (
    <div className="container ">
      <form onSubmit={handlesubmit}>
        <div className="form-group">
          <label htmlFor="employe">Employe</label>
          <select className="form-control" id="employe" ref={employe_id}>
            {employes.map((employe) => {
              return (
                <option value={employe.id} key={employe.id}>
                  {employe.nom} {employe.prenom}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="employe">Formation</label>
          <Select
            options={options}
            id="employe"
            ref={formation_id}
            isMulti
            onChange={(selectedOptions) =>
              setSelectedFormations(selectedOptions)
            }
            className="basic-multi-select"
            classNamePrefix="select"
          />
        </div>
        <button type="submit" className="btn btn-primary mt-2">
          Ajouter
        </button>
      </form>
    </div>
  );
}

export default AddFormation;
