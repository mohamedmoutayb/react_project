import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchParticipations } from "../../actions/participations/actions";
import { fetchEmployes } from "../../actions/employes/actions";
import { fetchFormations } from "../../actions/formations/actions";
import { Link } from "react-router-dom";
import Modal from "../Modal/Modal";

export default function ListParticipation() {
  const dispatch = useDispatch();
  const participations = useSelector(
    (state) => state.participations.participations
  );
  const employes = useSelector((state) => state.employes.employes);
  const formations = useSelector((state) => state.formations.formations);
  // const participation = participations.map((participation) => {
  //   return participation.formation_id;
  // });
  useEffect(() => {
    dispatch(fetchParticipations());
    dispatch(fetchFormations());
    dispatch(fetchEmployes());
  }, [dispatch]);

  const getEmployeById = (id) => {
    return employes.find((emp) => emp.id == id);
  };

  const getFormationsByIds = (ids) => {
    // if (participation.length === 1) {
    //   return formations.find((form) => form.id == ids);
    // }
    return ids.map((id) => formations.find((form) => form.id == id));
  };

  return (
    <div className="container mt-3">
      <Link className="btn btn-primary m-3" to={"/ajouter-participation"}>
        Ajouter une participation
      </Link>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nom complet</th>
            <th scope="col">Formations</th>
            <th scope="col" colSpan={2}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {participations.map((participation, key) => (
            <tr key={key}>
              <th scope="row">{participation.id}</th>
              <td>
                {getEmployeById(participation.employe_id)?.nom}{" "}
                {getEmployeById(participation.employe_id)?.prenom}
              </td>
              <td>
                {getFormationsByIds(participation.formation_id)?.map(
                  (formation, index) => (
                    <>
                      {formation && <li key={index}>{formation.intitule}</li>}
                    </>
                  )
                )}
              </td>
              <td>
              <Modal id={participation.id} />
              </td>
              <td>
                <Link
                  to={`/${participation.id}/modifier-participation`}
                  className="btn btn-success"
                >
                  Modifier
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
