import { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { updateParticipation } from "../../actions/participations/actions";
import Select from "react-select";

function UpdateParticipation() {
  const participations = useSelector(
    (state) => state.participations.participations
  );
  const employes = useSelector((state) => state.employes.employes);
  const formations = useSelector((state) => state.formations.formations);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { parId } = useParams();
  const employe_id = useRef();
  const [selectedFormations, setSelectedFormations] = useState([]);

  const participation = participations.find(
    (participation) => participation.id == parId
  );

  console.log(participation);

  const { id: vid, date_participation: vdate_participation } =
    participation || {};

  const handlesubmit = (e) => {
    e.preventDefault();
    const data = {
      id: vid,
      employe_id: employe_id.current.value,
      formation_id: selectedFormations.map((option) => option.value),
      date_participation: vdate_participation,
    };
    dispatch(updateParticipation(vid, data));
    navigate("/participations");
    // console.log(data)
  };

  const options = formations
  .filter(formation => formation.status != "Acheevées")
  .map(formation => ({
    value: formation.id,
    label: formation.intitule
  }));

  return (
    <div className="container">
      <form onSubmit={handlesubmit}>
        <div className="form-group">
          <label htmlFor="employe">Employe</label>
          <select
            className="form-control"
            id="employe"
            defaultValue={participation ? participation.employe_id : ""}
            ref={employe_id}
          >
            {employes.map((employe) => (
              <option value={employe.id} key={employe.id}>
                {employe.nom} {employe.prenom}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="formation">Formation</label>
          <Select
            options={options}
            id="formation"
            defaultValue={
              participation
                ? participation.formation_id.map((id) => ({
                    value: id,
                    label: formations.find((form) => form.id === id).intitule,
                  }))
                : []
            }
            isMulti
            onChange={(selectedOptions) =>
              setSelectedFormations(selectedOptions)
            }
            className="basic-multi-select"
            classNamePrefix="select"
          />
        </div>
        <button type="submit" className="btn btn-primary mt-2">
          Submit
        </button>
      </form>
    </div>
  );
}

export default UpdateParticipation;
