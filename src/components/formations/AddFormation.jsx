import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addFormation } from "../../actions/formations/actions";

function AddFormation() {
  const formations = useSelector((state) => state.formations.formations);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const intitule = useRef();
  const date_debut = useRef();
  const date_fin = useRef();
  const status = useRef();
  const [errors, setErrors] = useState({});
  const [isFormValid, setIsFormValid] = useState(false);

  const validateRequiredElement = (ref) => {
    if (ref.current.value.trim() === "") {
      setErrors((prevState) => {
        return { ...prevState, ...{ [ref.current.id]: "field required" } };
      });
      setIsFormValid(false);
    }
  };

  const validateForm = () => {
    let isFormValid = true;
    setErrors([]);
    validateRequiredElement(intitule);
    validateRequiredElement(date_debut);
    validateRequiredElement(date_fin);
    validateRequiredElement(status);

    const debutDate = new Date(date_debut.current.value);
    const finDate = new Date(date_fin.current.value);
    const currentDate = new Date();

    const currentDateFormatted = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      currentDate.getDate()
    );

    if (debutDate.getTime() <= currentDateFormatted.getTime()) {
      setErrors((prevState) => ({
        ...prevState,
        date_debut: "La date de début doit être supérieure à la date actuelle",
      }));
      isFormValid = false;
    }

    if (finDate.getTime() <= debutDate.getTime()) {
      setErrors((prevState) => ({
        ...prevState,
        date_fin: "La date de fin doit être supérieure à la date de début",
      }));
      isFormValid = false;
    }
    // console.log(debutDate)

    setIsFormValid(isFormValid);
    return isFormValid;
  };
  useEffect(() => {
    const today = new Date();
    const formattedToday = `${today.getFullYear()}-${(today.getMonth() + 1)
      .toString()
      .padStart(2, "0")}-${today.getDate().toString().padStart(2, "0")}`;
    // console.log(date_debut.current.value === formattedToday);
    if (date_debut.current.value === formattedToday) {
      status.current.value = "En cours";
    }
  }, [date_debut.current?.value]);

  const handlechange = () => {
    validateForm();
  };
  const getError = (fieldName) => {
    return errors[fieldName];
  };
  const hasError = (fieldName) => {
    return getError(fieldName) !== undefined;
  };
  const displayError = (ref) => {
    if (ref.current !== undefined) {
      const fieldName = ref.current.id;
      if (hasError(fieldName)) {
        ref.current.style.border = "1px solid red";
        return <div className={"text-danger"}>{getError(fieldName)}</div>;
      }
      ref.current.style.border = "1.5px solid green";
    }
  };
  const nextId = () => {
    if (formations.length === 0) {
      return 1;
    } else {
      return Math.max(...formations.map((form) => form.id)) + 1;
    }
  };
  const handlesubmit = (e) => {
    e.preventDefault();
    if (validateForm()) {
      const data = {
        id: nextId(),
        intitule: intitule.current.value,
        date_debut: date_debut.current.value,
        date_fin: date_fin.current.value,
        status: status.current.value,
        annuler: false,
      };
      console.log(data);
      dispatch(addFormation(data));
      navigate("/formations");
    }
  };

  return (
    <div className="container ">
      <form onSubmit={handlesubmit} onChange={handlechange}>
        <div className="form-group">
          <label htmlFor="intitule">Intitule</label>
          <input
            type="text"
            className="form-control"
            id="intitule"
            placeholder=""
            ref={intitule}
          />
          {displayError(intitule)}
        </div>
        <div className="form-group">
          <label htmlFor="date_debut">Date début</label>
          <input
            type="date"
            className="form-control"
            id="date_debut"
            placeholder=""
            ref={date_debut}
          />
          {displayError(date_debut)}
        </div>
        <div className="form-group">
          <label htmlFor="date_fin">Date fin</label>
          <input
            type="date"
            className="form-control"
            id="date_fin"
            placeholder=""
            ref={date_fin}
          />
          {displayError(date_fin)}
        </div>
        <div className="form-group">
          <label htmlFor="status">Status</label>
          <select className="form-control" id="status" disabled ref={status}>
            <option value={"Programmer"}>Programmer</option>
            <option value={"En cours"}>En cours</option>
          </select>
          {displayError(status)}
        </div>
        <button
          type="submit"
          disabled={!isFormValid}
          className="btn btn-primary mt-2"
        >
          Submit
        </button>
      </form>
    </div>
  );
}

export default AddFormation;
