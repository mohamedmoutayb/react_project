import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { updateFormation } from "../../actions/formations/actions";

function UpdateFormation() {
  const formations = useSelector((state) => state.formations.formations);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { forId } = useParams();
  const id = useRef();
  const intitule = useRef();
  const date_debut = useRef();
  const date_fin = useRef();
  // const status = useRef();
  console.log(typeof forId);
  const formation = formations.filter((formation) => formation.id == forId);
  const [formatione] = formation;
  const {
    id: vid,
    intitule: vintitule,
    date_debut: vdate_debut,
    date_fin: vdate_fin,
    status: vstatus,
    annuler:vannuler
  } = formatione;

  const handlesubmit = (e) => {
    e.preventDefault();
    // console.log(employee)
    const data = {
      id: vid,
      intitule: intitule.current.value,
      date_debut: date_debut.current.value,
      date_fin: date_fin.current.value,
      status: vstatus,
      annuler:vannuler
    };
    // console.log(data);
    dispatch(updateFormation(vid,data))
    navigate('/formations')
  };
  return (
    <div>
      <div className="container ">
        <form onSubmit={handlesubmit}>
          <div className="form-group">
            <label htmlFor="id">Id</label>
            <input
              type="text"
              className="form-control"
              id="id"
              defaultValue={vid}
              placeholder=""
              ref={id}
              disabled
            />
          </div>
          <div>
            <label htmlFor="intitule">Intitule</label>
            <input
              type="text"
              className="form-control"
              id="intitule"
              placeholder=""
              defaultValue={vintitule}
              ref={intitule}
            />
          </div>
          <div className="form-group">
            <label htmlFor="date_debut">Date debut</label>
            <input
              type="date"
              className="form-control"
              id="date_debut"
              placeholder=""
              defaultValue={vdate_debut}
              ref={date_debut}
            />
          </div>
          <div className="form-group">
            <label htmlFor="date_fin">Date fin</label>
            <input
              type="date"
              className="form-control"
              id="date_fin"
              placeholder=""
              defaultValue={vdate_fin}
              ref={date_fin}
            />
          </div>
          {/* <div className="form-group">
            <label htmlFor="diplome">Status</label>
            <select
              className="form-control"
              id="diplome"
              defaultValue={vstatus}
              ref={status}
            >
              <option value={"Programmer"}>Programmer</option>
              <option value={"En Cours"}>En Cours</option>
              <option value={"Acheevées"}>Acheevées</option>
              <option value={"Annuler"}>Annuler</option>
            </select>
          </div> */}
          <button type="submit" className="btn btn-primary mt-2">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default UpdateFormation;
