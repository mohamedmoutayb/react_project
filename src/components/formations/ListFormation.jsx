import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useSearchParams } from "react-router-dom";
import {
  // deleteFormation,
  fetchFormations,
  updateFormation,
} from "../../actions/formations/actions";
import CancelModal from "../Modal/CancelModal";
// import Modal from '../Modal/Modal'

export default function ListFormation() {
  const vdate_debut = useRef();
  const vdate_fin = useRef();
  const vstatus = useRef();
  const dispatch = useDispatch();
  let formationsList = useSelector((state) => state.formations.formations);
  let formations = formationsList.filter(formations => formations.annuler == false)
  console.log(formations);

  const calculateStatus = (date_debut, date_fin) => {
    const today = new Date();
    const formattedDateDebut = new Date(
      date_debut.split("/").reverse().join("-")
    );
    const formattedDateFin = new Date(date_fin.split("/").reverse().join("-"));

    if (today >= formattedDateDebut && today <= formattedDateFin) {
      return "En Cours";
    } else if (today > formattedDateFin) {
      return "Acheevées";
    } else if (today < formattedDateDebut) {
      return "Programmer";
    }
  };

  useEffect(() => {
    formations.forEach((formation) => {
      const status = calculateStatus(formation.date_debut, formation.date_fin);
      if (status !== formation.status) {
        const updatedFormation = { ...formation, status };
        dispatch(updateFormation(formation.id, updatedFormation));
      }
    });
  }, [formations, dispatch]);

  const [searchParams, setSearchParams] = useSearchParams({
    date_debut: "",
    date_fin: "",
    status: "",
  });
  let date_debut = searchParams.get("date_debut");
  let date_fin = searchParams.get("date_fin");
  let status = searchParams.get("status");

  formations =
    date_debut && date_fin
      ? formations.filter(
          (formation) =>
            formation.date_debut >= date_debut && formation.date_fin <= date_fin
        )
      : formations;
  formations = status
    ? formations.filter((formation) => formation.status === status)
    : formations;

  const clear = (e) => {
    e.preventDefault();
    vdate_debut.current.value = "";
    vdate_fin.current.value = "";
    vstatus.current.value = "Programmer";
    date_debut = setSearchParams((prev) => {
      prev.set("date_debut", "");
      return prev;
    });
    date_fin = setSearchParams((prev) => {
      prev.set("date_fin", "");
      return prev;
    });
    status = setSearchParams((prev) => {
      prev.set("status", "");
      return prev;
    });
    console.log(status);
  };
  useEffect(() => {
    dispatch(fetchFormations());
  }, [dispatch]);

  // const handledelete = (e) => {
  //   e.preventDefault();
  //   const id = e.currentTarget.value;
  //   console.log(id);
  //   dispatch(deleteFormation(id));
  // };
  const handlecancel = (e) => {
    e.preventDefault();
    const id = e.currentTarget.value;
    const annuler = true;
    formations.map((formation) => {
      if (formation.id == id) {
        const updatedFormation = { ...formation, annuler };
        dispatch(updateFormation(id, updatedFormation));
      }
    });
    // dispatch(updateFormation(id));
  };

  return (
    <div className="container mt-3">
      <div className="my-3">
        <form>
          <div className="form-group">
            <label htmlFor="formation">Date Debut</label>
            <input
              type="date"
              className="form-control"
              id="date_debut"
              ref={vdate_debut}
              onChange={(e) =>
                setSearchParams((prev) => {
                  prev.set("date_debut", e.target.value);
                  return prev;
                })
              }
            />
          </div>
          <div className="form-group">
            <label htmlFor="date_fin">Date Fin</label>
            <input
              type="date"
              className="form-control"
              id="date_fin"
              ref={vdate_fin}
              onChange={(e) =>
                setSearchParams((prev) => {
                  prev.set("date_fin", e.target.value);
                  return prev;
                })
              }
            />
          </div>
          <div className="form-group">
            <label htmlFor="status">Status</label>
            <select
              className="form-control"
              name="status"
              id="status"
              ref={vstatus}
              onChange={(e) =>
                setSearchParams((prev) => {
                  prev.set("status", e.target.value);
                  return prev;
                })
              }
            >
              <option value={"Programmer"}>Programmer</option>
              <option value={"En Cours"}>En Cours</option>
              <option value={"Acheevées"}>Acheevées</option>
            </select>
          </div>
          <div className="form-group">
            <button className="btn btn-primary mt-2" onClick={clear}>
              Effacer
            </button>
          </div>
        </form>
      </div>
      <div>
        <Link className="btn btn-primary m-3" to={"/ajouter-formation"}>
          Ajouter une formation
        </Link>
        <CancelModal  />
      </div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Intitule</th>
            <th scope="col">Date de début</th>
            <th scope="col">Date de fin</th>
            <th scope="col">Status</th>
            <th scope="col" colSpan={2}>
              Actions
            </th>
          </tr>
        </thead>
        <tbody>
          {formations.map((formation, key) => (
            <tr key={key}>
              <th scope="row">{formation.id}</th>
              <td>{formation.intitule}</td>
              <td>{formation.date_debut}</td>
              <td>{formation.date_fin}</td>
              <td>{formation.status}</td>
              {/* <td>
                <button
                  className="btn btn-danger"
                  value={formation.id}
                  onClick={handledelete}
                >
                  Supprimer
                </button>
              </td> */}
              <td>
                <button
                  className="btn btn-danger"
                  value={formation.id}
                  onClick={handlecancel}
                >
                  Annuler
                </button>
              </td>
              <td>
                <Link
                  to={`/${formation.id}/modifier-formation`}
                  className="btn btn-success"
                >
                  Modifier
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
