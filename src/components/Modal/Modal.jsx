import { useState } from "react";
import styles from "./Modal.module.css";
import { useSelector } from "react-redux";

export default function Modal({ id }) {
  const participations = useSelector(
    (state) => state.participations.participations
  );
  const participation = participations.find(
    (participation) => participation.id == id
  );
  const { employe_id, formation_id } = participation;

  const employes = useSelector((state) => state.employes.employes);
  const employe = employes.find((employe) => employe.id == employe_id);

  const formations = useSelector((state) => state.formations.formations);
  const formation = formation_id.map((id) =>
    formations.find((formation) => formation.id === id)
  );

  //   if(formations && formation == undefined){
  //     const formation =
  //   }


  console.log(formation);

  const [modal, setModal] = useState(false);
  const toggleModal = () => {
    setModal(!modal);
  };

  if (modal) {
    document.body.classList.add("active-modal");
  } else {
    document.body.classList.remove("active-modal");
  }

  return (
    <>
      <button onClick={toggleModal} className="btn btn-success">
        Details
      </button>

      {modal && (
        <div className={modal}>
          <div onClick={toggleModal} className={styles.overlay}></div>
          <div className={styles.modalContent}>
            <h4>Details</h4>
            <p>Nom: {employe.nom}</p>
            <p>Prenom: {employe.prenom}</p>
            <p>CIN: {employe.cin}</p>
            <p>Formations:</p>
            <ul>
              {formation.map((form) => (
                <li key={form.id}>
                  <p>Intitule: {form.intitule}</p>
                  <p>Date Debut: {form.date_debut}</p>
                  <p>Date Fin: {form.date_fin}</p>
                  <p>Status: {form.status}</p>
                </li>
              ))}
            </ul>
            <button className={styles.closeModal} onClick={toggleModal}>
              X
            </button>
          </div>
        </div>
      )}
    </>
  );
}
