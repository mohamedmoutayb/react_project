import { useState } from "react";
import styles from "./CancelModal.module.css";
import { useSelector } from "react-redux";

export default function CancelModal() {
  let formationsList = useSelector((state) => state.formations.formations);
  let formations = formationsList.filter(
    (formations) => formations.annuler == true
  );
  // formations.map(formation => {
  //   console.log(formation.id)
  // })

  const [modal, setModal] = useState(false);
  const toggleModal = () => {
    setModal(!modal);
  };

  if (modal) {
    document.body.classList.add("active-modal");
  } else {
    document.body.classList.remove("active-modal");
  }

  return (
    <>
      <button onClick={toggleModal} className="btn btn-success">
        Les formations annulées
      </button>

      {modal && (
        <div className={modal}>
          <div onClick={toggleModal} className={styles.overlay}></div>
          <div className={styles.modalContent}>
            <h6>Les formations annulées</h6>
            <div className={styles.tableContainer}>
              <table className="table">
                <thead>
                  <th>Id</th>
                  <th>Intitule</th>
                  <th>Date Debut</th>
                  <th>Date Fin</th>
                </thead>
                <tbody>
                  {formations.map((formation) => (
                    <tr key={formation.id}>
                      <td>{formation.id}</td>
                      <td>{formation.intitule}</td>
                      <td>{formation.date_debut}</td>
                      <td>{formation.date_fin}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <button className={styles.closeModal} onClick={toggleModal}>
              X
            </button>
          </div>
        </div>
      )}
    </>
  );
}
