import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {  fetchEmployes } from "../../actions/employes/actions";
import { Link, useSearchParams } from "react-router-dom";

export default function ListEmploye() {
  const dispatch = useDispatch();
  const employes = useSelector((state) => state.employes.employes);

  const [searchParams, setSearchParams] = useSearchParams({ formation: "" });
  const formation = searchParams.get("formation");
  // console.log(formation)
  const employees =
    formation.length > 0
      ? employes.filter((employe) => employe.nmbr_formation == formation)
      : employes;

  // const search = () => {
  // };
  useEffect(() => {
    dispatch(fetchEmployes());
  }, [dispatch]);

  // const handledelete = (e) => {
  //   e.preventDefault();
  //   const id = e.currentTarget.value;
  //   console.log(id);
  //   dispatch(deleteEmploye(id));
  // };

  return (
    <div className="container mt-3">
      <div className="my-3">
        <form>
          <div className="form-group">
            <label htmlFor="formation">Nombre de formation : </label>
            <input
              type="text"
              className="form-control"
              id="formation"
              onChange={(e) =>
                setSearchParams(
                  (prev) => {
                    prev.set("formation", e.target.value);
                    return prev;
                  },
                  { replace: true }
                )
              }
            />
          </div>
        </form>
      </div>
      <Link className="btn btn-primary m-3" to={"/ajouter-employe"}>
        Ajouter un employer
      </Link>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nom</th>
            <th scope="col">Prenom</th>
            <th scope="col">Diplome</th>
            <th scope="col">CIN</th>
            <th scope="col">Telephone</th>
            <th scope="col">Nombre de formation</th>
            <th scope="col" >
              Actions
            </th>
          </tr>
        </thead>
        <tbody>
          {employees.map((employe, key) => (
            <tr key={key}>
              <th scope="row">{employe.id}</th>
              <td>{employe.nom}</td>
              <td>{employe.prenom}</td>
              <td>{employe.diplome}</td>
              <td>{employe.cin}</td>
              <td>{employe.telephone}</td>
              <td>{employe.nmbr_formation}</td>
              {/* <td>
                <button
                  className="btn btn-danger"
                  value={employe.id}
                  onClick={handledelete}
                >
                  Supprimer
                </button>
              </td> */}
              <td>
                <Link
                  to={`/${employe.id}/modifier-employe`}
                  className="btn btn-success"
                >
                  Modifier
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
