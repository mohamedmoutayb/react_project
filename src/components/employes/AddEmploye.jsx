import { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addEmploye } from "../../actions/employes/actions";
import { useNavigate } from "react-router-dom";

function AddEmploye() {
  const { employes } = useSelector((state) => state);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const nom = useRef();
  const prenom = useRef();
  const diplome = useRef();
  const cin = useRef();
  const telephone = useRef();
  const [errors, setErrors] = useState({});
  const [isFormValid, setIsFormValid] = useState(false);

  const validateRequiredElement = (ref) => {
    if (ref.current.value.trim() === "") {
      setErrors((prevState) => {
        return { ...prevState, ...{ [ref.current.id]: "field required" } };
      });
      setIsFormValid(false);
    }
  };

  const validateForm = () => {
    const telephoneValue = telephone.current.value
    const cinValue = cin.current.value
    let isFormValid = true;
    setErrors([]);
    validateRequiredElement(nom);
    validateRequiredElement(prenom);
    validateRequiredElement(cin);
    validateRequiredElement(telephone);
    if (!telephoneValue.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)) {
      setErrors(prevState => {
          return {
              ...prevState,
              ...{telephone: 'Telephone incorrect'}
          }
      })
      isFormValid = false
  }
    if (!cinValue.match(/^[A-Za-z]{2}\d{6}$/)) {
      setErrors(prevState => {
          return {
              ...prevState,
              ...{cin: 'CIN incorrect'}
          }
      })
      isFormValid = false
  }

    setIsFormValid(isFormValid);
    return isFormValid;
  };

  const handlechange = () => {
    validateForm()
  };
  const getError = (fieldName) => {
    return errors[fieldName]
  }
  const hasError = (fieldName) => {
      return getError(fieldName) !== undefined
  }
  const displayError = (ref) => {
      if (ref.current !== undefined) {
          const fieldName = ref.current.id
          if (hasError(fieldName)) {
              ref.current.style.border = '1px solid red'
              return <div className={'text-danger'}>{getError(fieldName)}</div>
          }
          ref.current.style.border = '1.5px solid green'
      }
  }
  const nextId = () => {
    if (employes.employes.length === 0) {
      return 1;
    } else {
      return parseInt(Math.max(employes.employes.map((emp) => emp.id)) + 1);
    }
  };
  const handlesubmit = (e) => {
    e.preventDefault();
    const data = {
      id: nextId(),
      nom: nom.current.value,
      prenom: prenom.current.value,
      diplome: diplome.current.value,
      cin: cin.current.value,
      telephone: telephone.current.value,
      nmbr_formation: 0,
    };
    console.log(data);
    dispatch(addEmploye(data));
    navigate("/employes");
  };

  return (
    <div className="container ">
      <form onSubmit={handlesubmit} onChange={handlechange}>
        <div className="form-group">
          <label htmlFor="nom">Nom</label>
          <input
            type="text"
            className="form-control"
            id="nom"
            placeholder=""
            ref={nom}
          />
          {displayError(nom)}
        </div>
        <div className="form-group">
          <label htmlFor="prenom">Prenom</label>
          <input
            type="text"
            className="form-control"
            id="prenom"
            placeholder=""
            ref={prenom}
          />
          {displayError(prenom)}
        </div>
        <div className="form-group">
          <label htmlFor="diplome">Diplome</label>
          <select className="form-control" id="diplome" ref={diplome}>
            <option value={"Technicien"}>Technicien</option>
            <option value={"Technicien Spécialisé"}>
              Technicien Spécialisé
            </option>
            <option value={"Ingénieure d'etat"}>Ingénieure d&apos;etat</option>
          </select>
          {displayError(diplome)}
        </div>
        <div className="form-group">
          <label htmlFor="cin">CIN</label>
          <input
            type="text"
            className="form-control"
            id="cin"
            placeholder=""
            ref={cin}
          />
          {displayError(cin)}
        </div>
        <div className="form-group">
          <label htmlFor="telephone">Téléphone</label>
          <input
            type="text"
            className="form-control"
            id="telephone"
            placeholder=""
            ref={telephone}
            />
            {displayError(telephone)}
        </div>
        <button type="submit" disabled={!isFormValid} className="btn btn-primary mt-2">
          Submit
        </button>
      </form>
    </div>
  );
}

export default AddEmploye;
