import {  useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { updateEmploye } from "../../actions/employes/actions";

function UpdateEmploye() {
  const employes = useSelector((state) => state.employes.employes);
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { emId } = useParams();
  const id = useRef();
  const nom = useRef();
  const prenom = useRef();
  const diplome = useRef();
  const cin = useRef();
  const telephone = useRef();

  const employe = employes.filter((employe) => employe.id == emId);
  const [employee] = employe;
  const { cin: vcin,diplome: vdiplome ,id: vid ,nmbr_formation: vformation ,nom: vnom ,prenom: vprenom ,telephone: vtelephone } = employee;

  const handlesubmit = (e) => {
    e.preventDefault();
    // console.log(employee)
    const data = {
        id:vid,
        nom : nom.current.value,
        prenom : prenom.current.value,
        diplome : diplome.current.value,
        cin : cin.current.value,
        telephone : telephone.current.value,
        nmbr_formation: vformation
    }
    console.log(data)
    dispatch(updateEmploye(vid,data))
    navigate('/employes')
  };
  return (
    <div>
      <div className="container ">
        <form onSubmit={handlesubmit}>
          <div className="form-group">
            <label htmlFor="id">Id</label>
            <input
              type="text"
              className="form-control"
              id="id"
              defaultValue={vid}
              placeholder=""
              ref={id}
              disabled
            />
          </div>
          <div>
            <label htmlFor="nom">Nom</label>
            <input
              type="text"
              className="form-control"
              id="nom"
              placeholder=""
              defaultValue={vnom}
              ref={nom}
            />
          </div>
          <div className="form-group">
            <label htmlFor="prenom">Prenom</label>
            <input
              type="text"
              className="form-control"
              id="prenom"
              placeholder=""
              defaultValue={vprenom}
              ref={prenom}
            />
          </div>
          <div className="form-group">
            <label htmlFor="diplome">Diplome</label>
            <select className="form-control" id="diplome" defaultValue={vdiplome} ref={diplome}>
              <option value={"Technicien"}>Technicien</option>
              <option value={"Technicien Spécialisé"}>
                Technicien Spécialisé
              </option>
              <option value={"Ingénieure d'etat"}>
                Ingénieure d&apos;etat
              </option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="cin">CIN</label>
            <input
              type="text"
              className="form-control"
              id="cin"
              placeholder=""
              defaultValue={vcin}
              ref={cin}
            />
          </div>
          <div className="form-group">
            <label htmlFor="telephone">Téléphone</label>
            <input
              type="text"
              className="form-control"
              id="telephone"
              placeholder=""
              defaultValue={vtelephone}
              ref={telephone}
            />
          </div>
          <div className="form-group">
            <label htmlFor="telephone">Nombre de formation</label>
            <input
              type="text"
              className="form-control"
              id="telephone"
              placeholder=""
              defaultValue={vformation}
              disabled
            />
          </div>
          <button type="submit" className="btn btn-primary mt-2">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default UpdateEmploye;
