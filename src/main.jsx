import ReactDOM from 'react-dom/client'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';
import App from './App.jsx'
import './index.css'
import { Provider } from 'react-redux';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import store from './store.js';
import ListEmploye from './components/employes/ListEmploye.jsx';
import AddEmploye from './components/employes/AddEmploye.jsx';
import UpdateEmploye from './components/employes/UpdateEmploye.jsx';
import ListFormation from './components/formations/ListFormation.jsx';
import AddFormation from './components/formations/AddFormation.jsx';
import UpdateFormation from './components/formations/UpdateFormation.jsx';
import ListParticipation from './components/participations/ListParticipation.jsx';
import AddParticipation from './components/participations/AddParticipation.jsx';
import UpdateParticipation from './components/participations/UpdateParticipation.jsx';

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <BrowserRouter>
        <Routes>
            <Route path='/' element={<App/>}>
                  <Route index path='/employes' element={<ListEmploye/>}/>
                  <Route path='/ajouter-employe' element={<AddEmploye/>}/>
                  <Route path='/:emId/modifier-employe' element={<UpdateEmploye/>}/>
                  <Route path='/formations' element={<ListFormation/>}/>
                  <Route path='/ajouter-formation' element={<AddFormation/>}/>
                  <Route path='/:forId/modifier-formation' element={<UpdateFormation/>}/>
                  <Route path='/participations' element={<ListParticipation/>}/>
                  <Route path='/ajouter-participation' element={<AddParticipation/>}/>
                  <Route path='/:parId/modifier-participation' element={<UpdateParticipation/>}/>
            </Route>
        </Routes>
    </BrowserRouter>
  </Provider>
)
