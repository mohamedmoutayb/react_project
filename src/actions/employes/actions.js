import { FETCH_EMPLOYES_REQUEST, FETCH_EMPLOYES_SUCCESS, FETCH_EMPLOYES_FAILURE, ADD_EMPLOYE,
DELETE_EMPLOYE,
UPDATE_EMPLOYE} from './type.js'; 

import API from '../../API/services/EmployeService.js';

const fetchEmployesRequest = () => ({
    type: FETCH_EMPLOYES_REQUEST
});

const fetchEmployesSuccess = (employes) => ({
    type: FETCH_EMPLOYES_SUCCESS,
    payload: employes
});

const fetchEmployesFailure = (error) => ({
    type: FETCH_EMPLOYES_FAILURE,
    payload: error
});

export const fetchEmployes = () => {
    return async (dispatch) => {
        dispatch(fetchEmployesRequest());
        try {
            const response = await API.getAll(); 
            console.log(response.data);
            dispatch(fetchEmployesSuccess(response.data)); 
        } catch (error) {
            dispatch(fetchEmployesFailure(error.message));
        }
    };
};

export const addEmploye = (employe) => {
    return async (dispatch) => {
        try {
            const response = await API.create(employe)
            console.log(response )
            dispatch({
                type: ADD_EMPLOYE,
                payload: response.data,
            })
        } catch (error) {
            return error
        }
    }
};
export const updateEmploye = (id,employe) => {
    return async (dispatch) => {
        try {
            const response = await API.update(id,employe)
            console.log(response)
            dispatch({
                type: UPDATE_EMPLOYE,
                payload: employe,
            })
        } catch (error) {
            return error
        }
    }
};

export const deleteEmploye = (id) => {
    return async (dispatch) => {
        try {
            await API.delete(id)
            dispatch({
                type: DELETE_EMPLOYE,
                payload: id
            })
        } catch (error) {
            return error
        }
    }
}
