import { FETCH_PARTICIPATIONS_REQUEST, FETCH_PARTICIPATIONS_SUCCESS, FETCH_PARTICIPATIONS_FAILURE, ADD_PARTICIPATION,
DELETE_PARTICIPATION,
UPDATE_PARTICIPATION} from './type.js'; 

import API from '../../API/services/ParticipationService.js';

const fetchParticipationsRequest = () => ({
    type: FETCH_PARTICIPATIONS_REQUEST
});

const fetchParticipationsSuccess = (participations) => ({
    type: FETCH_PARTICIPATIONS_SUCCESS,
    payload: participations
});

const fetchParticipationsFailure = (error) => ({
    type: FETCH_PARTICIPATIONS_FAILURE,
    payload: error
});

export const fetchParticipations = () => {
    return async (dispatch) => {
        dispatch(fetchParticipationsRequest());
        try {
            const response = await API.getAll(); 
            console.log(response.data);
            dispatch(fetchParticipationsSuccess(response.data)); 
        } catch (error) {
            dispatch(fetchParticipationsFailure(error.message));
        }
    };
};

export const addParticipation = (participation) => {
    return async (dispatch) => {
        try {
            const response = await API.create(participation)
            console.log(response )
            dispatch({
                type: ADD_PARTICIPATION,
                payload: response.data,
            })
        } catch (error) {
            return error
        }
    }
};
export const updateParticipation = (id,participation) => {
    return async (dispatch) => {
        try {
            const response = await API.update(id,participation)
            console.log(response)
            dispatch({
                type: UPDATE_PARTICIPATION,
                payload: participation,
            })
        } catch (error) {
            return error
        }
    }
};

export const deleteParticipation = (id) => {
    return async (dispatch) => {
        try {
            await API.delete(id)
            dispatch({
                type: DELETE_PARTICIPATION,
                payload: id
            })
        } catch (error) {
            return error
        }
    }
}
