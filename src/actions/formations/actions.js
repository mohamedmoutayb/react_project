import { FETCH_FORMATIONS_REQUEST, FETCH_FORMATIONS_SUCCESS, FETCH_FORMATIONS_FAILURE, ADD_FORMATION,
DELETE_FORMATION,
UPDATE_FORMATION} from './type.js'; 

import API from '../../API/services/FormationService.js';

const fetchFormationsRequest = () => ({
    type: FETCH_FORMATIONS_REQUEST
});

const fetchFormationsSuccess = (formations) => ({
    type: FETCH_FORMATIONS_SUCCESS,
    payload: formations
});

const fetchFormationsFailure = (error) => ({
    type: FETCH_FORMATIONS_FAILURE,
    payload: error
});

export const fetchFormations = () => {
    return async (dispatch) => {
        dispatch(fetchFormationsRequest());
        try {
            const response = await API.getAll(); 
            console.log(response.data);
            dispatch(fetchFormationsSuccess(response.data)); 
        } catch (error) {
            dispatch(fetchFormationsFailure(error.message));
        }
    };
};

export const addFormation = (formation) => {
    return async (dispatch) => {
        try {
            const response = await API.create(formation)
            console.log(response )
            dispatch({
                type: ADD_FORMATION,
                payload: response.data,
            })
        } catch (error) {
            return error
        }
    }
};
export const updateFormation = (id,formation) => {
    return async (dispatch) => {
        try {
            const response = await API.update(id,formation)
            console.log(response)
            dispatch({
                type: UPDATE_FORMATION,
                payload: formation,
            })
        } catch (error) {
            return error
        }
    }
};



export const deleteFormation = (id) => {
    return async (dispatch) => {
        try {
            await API.delete(id)
            dispatch({
                type: DELETE_FORMATION,
                payload: id
            })
        } catch (error) {
            return error
        }
    }
}
