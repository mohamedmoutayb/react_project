import { combineReducers } from "redux";
import employesReducer from "./employesReducer";
import formationsReducer from "./formationsReducer";
import participationsReducer from "./participationsReducer";

const rootReducer = combineReducers({
    employes : employesReducer,
    formations : formationsReducer,
    participations : participationsReducer,
})

export default rootReducer