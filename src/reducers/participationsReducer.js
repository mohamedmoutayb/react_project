import {
  FETCH_PARTICIPATIONS_REQUEST,
  FETCH_PARTICIPATIONS_SUCCESS,
  FETCH_PARTICIPATIONS_FAILURE,
  ADD_PARTICIPATION,
  DELETE_PARTICIPATION,
  UPDATE_PARTICIPATION
} from "../actions/participations/type.js";

const initialState = {
  participations: [],
  loading: false,
  error: null,
};

const participationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PARTICIPATIONS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_PARTICIPATIONS_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        loading: false,
        participations: action.payload,
        error: null,
      };

    case FETCH_PARTICIPATIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_PARTICIPATION:
      return {
      ...state,
      participations: [...state.participations, action.payload]
      }
    case DELETE_PARTICIPATION: 
        return {
          ...state,
          participations:[...state.participations.filter((participation) => participation.id != action.payload)]
        }
    case UPDATE_PARTICIPATION:
      return {
        ...state,
        participations: state.participations.map((participation) => {
          if (participation.id === action.payload.id) {
            return action.payload;
          }
          return participation;
        })
      };
    default:
      return state;
  }
};

export default participationsReducer;
