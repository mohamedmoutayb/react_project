import {
  FETCH_EMPLOYES_REQUEST,
  FETCH_EMPLOYES_SUCCESS,
  FETCH_EMPLOYES_FAILURE,
  ADD_EMPLOYE,
  DELETE_EMPLOYE,
  UPDATE_EMPLOYE
} from "../actions/employes/type.js";

const initialState = {
  employes: [],
  loading: false,
  error: null,
};

const employesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EMPLOYES_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_EMPLOYES_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        loading: false,
        employes: action.payload,
        error: null,
      };

    case FETCH_EMPLOYES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_EMPLOYE:
      return {
      ...state,
        employes: [...state.employes, action.payload]
      }
    case DELETE_EMPLOYE: 
        return {
          ...state,
          employes:[...state.employes.filter((employe) => employe.id != action.payload)]
        }
    case UPDATE_EMPLOYE:
      return {
        ...state,
        employes: state.employes.map((employe) => {
          if (employe.id === action.payload.id) {
            return action.payload;
          }
          return employe;
        })
      };
    default:
      return state;
  }
};

export default employesReducer;
