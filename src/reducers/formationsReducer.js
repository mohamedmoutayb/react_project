import {
  FETCH_FORMATIONS_REQUEST,
  FETCH_FORMATIONS_SUCCESS,
  FETCH_FORMATIONS_FAILURE,
  ADD_FORMATION,
  DELETE_FORMATION,
  UPDATE_FORMATION
} from "../actions/formations/type.js";

const initialState = {
  formations: [],
  loading: false,
  error: null,
};

const formationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FORMATIONS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_FORMATIONS_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        loading: false,
        formations: action.payload,
        error: null,
      };

    case FETCH_FORMATIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_FORMATION:
      return {
      ...state,
      formations: [...state.formations, action.payload]
      }
    case DELETE_FORMATION: 
        return {
          ...state,
          formations:[...state.formations.filter((formation) => formation.id != action.payload)]
        }
    case UPDATE_FORMATION:
      return {
        ...state,
        formations: state.formations.map((formation) => {
          if (formation.id === action.payload.id) {
            return action.payload;
          }
          return formation;
        })
      };
    default:
      return state;
  }
};

export default formationsReducer;
